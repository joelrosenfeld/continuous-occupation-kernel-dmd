% This code was created by Joel Rosenfeld in 2021 to accompany his YouTube
% channel ThatMaththing (http://www.thatmaththing.com/
% If you use this code for a project, please credit Joel A. Rosenfeld
% Please also cite: 
% % "Dynamic Mode Decomposition for Continuous Time Systems with the Liouville Operator"
% % Joel A. Rosenfeld, Rushikesh Kamalapurkar, L. Forest Gruss, Taylor T. Johnson
% % https://arxiv.org/abs/1910.03977

tic
% Load the DATA!
    % This DATA comes from the DMD book by Nathan Kutz et al, and Data
    % Driven Science and Engineering by Brunton and Kutz
    
    load('DATA/FLUIDS/CYLINDER_ALL.mat');
    
    W = UALL; % I always put things in W. Each column is a state at that particular timestep.
        
    h = 0.02; 
    
    
    TotalSnapshots = size(W,2); % How many snapshots
    TotalKernels = size(W,2) - 1; % One less kernel than snapshots
% Define the Kernel Function
    WhichKernel = 1;
    if(WhichKernel == 1)
        mu = 500; % 0.7271 Appropriate Size between samples

        Kernel = @(x,y) exp(-1/mu*norm(x-y)^2); % This is the SLOW way to do things.
    elseif(WhichKernel == 2)
        mu = 1000; %7.2300e+04;
        Kernel = @(x,y) exp(1/mu*x'*y);
    elseif(WhichKernel == 3)
        mu = 1000; %7.2300e+04;
        Kernel = @(x,y) 1/mu*x'*y;
    end
    
% Trajectories

    U = zeros(TotalSnapshots-4,TotalSnapshots);
    
    SimpsonsVector = [1,4,2,4,1];
    
    U = [SimpsonsVector,zeros(1,TotalSnapshots-5)];
    
    for i = 2:TotalSnapshots-4
        U(i,:) = [zeros(1,i-1),SimpsonsVector,zeros(1,TotalSnapshots - (5 + i-1))];
    end

    
% Gram Matrix for Occupation Kernels

    GramKern = zeros(TotalSnapshots);
    
    for i = 1:TotalSnapshots
        for j = 1:TotalSnapshots
            GramKern(i,j) = Kernel(W(:,i),W(:,j));
        end
    end
    
    GramOccKern = h^2/9*U*GramKern*U';
    
% Interaction Matrix

    % A_f OccKern_i, OccKern_j = OccKern_i(traj_j(end)) -
    % OccKern_i(traj_j(1))
    
    % = int_0^T K(traj_2(end),traj_1(t)) - K(traj_2(1),traj_1(t)) dt
    
    InteractionMatrix = zeros(size(GramOccKern));
    
    InteractionHold = h/3*U*GramKern;
    for i = 1:size(GramOccKern,1)
        HoldFind = find(U(i,:));
        InteractionMatrix(:,i) = InteractionHold(:,HoldFind(end)) - InteractionHold(:,HoldFind(1));
    end
    
    %InteractionMatrix = InteractionMatrix.';
    
% EigenDecomposition

    [V,D] = eig(GramOccKern\InteractionMatrix);
    
    for i = 1:size(V,2)
        V(:,i) = V(:,i)/sqrt(V(:,i)'*GramOccKern*V(:,i));
    end
    
% Project the full state observable

    LiouvilleModes = ((GramOccKern*V)\(h/3*U*W'))';
    
    figure
    
    for i=1:length(LiouvilleModes(1,:))
        imshow(real(reshape(LiouvilleModes(:,i),199,[])),[],'Colormap',parula,'InitialMagnification',200);
        title('Liouville Modes');
        pause(0.05);
    end

% Initial Point Evaluation

    InitialEvaluation = V.'*h/3*U*GramKern(:,1);
    
%     InitialEvaluation = pinv(LiouvilleModes)*W(:,1);
% Reconstruction
%     Reconstruction = LiouvilleModes*diag(InitialEvaluation)*(diag(exp(D)).^(h*(0:(TotalSnapshots-1))));

    LiouvilleReconstructionFunction = @(t) LiouvilleModes*(InitialEvaluation.*exp(diag(D)*t));
toc


% Show Original



    figure
    
    for i=1:length(W(1,:))
        imshow(real(reshape(W(:,i),199,[])),[],'Colormap',parula,'InitialMagnification',200);
                title('Original');
        pause(0.02);
    end
% Show Reconstruction


    figure
        
    for i=1:length(Reconstruction(1,:))
        imshow(real(reshape(LiouvilleReconstructionFunction((i-1)*h),199,[])),[],'Colormap',parula,'InitialMagnification',200);
        title('Reconstruction');
        pause(0.02);
    end

